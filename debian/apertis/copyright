Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: debian/*
Copyright: 2017, Axel Beckert <abe@debian.org>
 2008-2017, Samuel Thibault <sthibault@debian.org>
 2004-2012, Peter Samuelson <peter@p12n.org>
 2004-2008, Guillem Jover <guillem@debian.org>
 1999-2002, Zephaniah E. Hull <warp@debian.org>
 1998, François Gouget <fgouget@mygale.org>
 1997-1999, James Troup <james@nocrew.org>
 1996, 1997, Martin Schulze <joey@debian.org>
License: GPL-2+

Files: *
Copyright: 1993       Andrew Haylett <ajh@gec-mrc.co.uk>
 1994-2000  Alessandro Rubini <rubini@linux.it>
 1998-1999  Ian Zimmerman <itz@rahul.net>
 2001-2012  Nico Schottelius <nico@schottelius.org>
License: GPL-2.0+

Files: debian/patches/007_doc_fix_001
Copyright: 1998-2001, Alessandro Rubini <rubini@linux.it>
License: GPL-2+

Files: debian/patches/010_ps2_rework.patch
Copyright: 2002, Zephaniah E. Hull <warp@debian.org>
 2001-2008, Nico Schottelius <nico-gpm2008 at schottelius.org>
 1998, 1999, Ian Zimmerman <itz@rahul.net>
 1994-2000, Alessandro Rubini <rubini@linux.it>
License: GPL

Files: scripts/*
Copyright: no-info-found
License: GPL-3+

Files: src/*
Copyright: 2001-2008, Nico Schottelius <nico-gpm2008 at schottelius.org>
License: GPL-2+

Files: src/daemon/gpm-killed.c
 src/daemon/gpm.c
Copyright: 2001-2008, Nico Schottelius <nico-gpm2008 at schottelius.org>
 1998, Ian Zimmerman <itz@rahul.net>
 1994-1999, Alessandro Rubini <rubini@linux.it>
 1993, Andreq Haylett <ajh@gec-mrc.co.uk>
License: GPL-2+

Files: src/daemon/processspecial.c
Copyright: 2001-2008, Nico Schottelius <nico-gpm2008 at schottelius.org>
 1998, Ian Zimmerman <itz@rahul.net>
 1994-1999, Alessandro Rubini <rubini@linux.it>
License: GPL-2+

Files: src/headers/gpm.h
Copyright: 2002-2008, Nico Schottelius <nico-gpm2008 at schottelius.org>
 1998, Ian Zimmerman <itz@rahul.net>
 1994, 1995, rubini@linux.it (Alessandro Rubini)
License: GPL-2+

Files: src/headers/gpmCfg.h
Copyright: 1998, Ian Zimmerman <itz@rahul.net>
 1994-1996, rubini@linux.it
License: GPL-2+

Files: src/headers/gpmInt.h
Copyright: 2001-2008, Nico Schottelius <nico-gpm2008 at schottelius.org>
 1998, Ian Zimmerman <itz@rahul.net>
 1994-1999, Alessandro Rubini <rubini@linux.it>
License: GPL-2+

Files: src/headers/synaptics.h
Copyright: 1999, hdavies@ameritech.net (Henry Davies
License: GPL-2+

Files: src/lib/*
Copyright: 2002, nico@schottelius.org (Nico Schottelius)
 1994, 1995, rubini@linux.it (Alessandro Rubini)
License: GPL-2+

Files: src/lib/libhigh.c
Copyright: 1994, 1995, 1998, rubini@linux.it (Alessandro Rubini)
License: GPL-2+

Files: src/lib/liblow.c
Copyright: 2001-2008, Nico Schottelius (nico-gpm2008 at schottelius.org)
 1998, Ian Zimmerman <itz@rahul.net>
 1994, 1995, rubini@linux.it (Alessandro Rubini)
License: GPL-2+

Files: src/lib/libxtra.c
Copyright: 1998, Ian Zimmerman <itz@rahul.net>
 1994, 1995, rubini@linux.it (Alessandro Rubini)
License: GPL-2+

Files: src/lib/report-lib.c
Copyright: 2001-2008, Nico Schottelius <nico-gpm2008 at schottelius.org>
License: GPL-2+

Files: src/mice.c
Copyright: 2002, Zephaniah E. Hull <warp@debian.org>
 2001-2008, Nico Schottelius <nico-gpm2008 at schottelius.org>
 1998, 1999, Ian Zimmerman <itz@rahul.net>
 1994-2000, Alessandro Rubini <rubini@linux.it>
 1993, Andrew Haylett <ajh@gec-mrc.co.uk>
License: GPL-2+

Files: src/prog/disable-paste.c
Copyright: 1998, Ian Zimmerman <itz@rahul.net>
License: GPL-2+

Files: src/prog/gpm-root.y
 src/prog/mev.c
Copyright: 1998, Ian Zimmerman <itz@rahul.net>
 1994, 1995, rubini@linux.it (Alessandro Rubini)
License: GPL-2+

Files: src/prog/hltest.c
Copyright: 1994, 1995, 1998, rubini@linux.it (Alessandro Rubini)
License: GPL-2+

Files: src/prog/mouse-test.c
Copyright: 2002-2008, Nico Schottelius <nico-gpm2008 at schottelius.org>
 1998, Ian Zimmerman <itz@rahul.net>
 1994, 1995, rubini@linux.it (Alessandro Rubini)
License: GPL-2+

Files: src/synaptics.c
Copyright: 2002, Linuxcare Inc.
 1999, hdavies@ameritech.net (Henry Davies)
License: GPL-2+

Files: src/twiddler.c
Copyright: 1994, 1995, 1998, rubini@linux.it (Alessandro Rubini)
License: GPL-2+

Files: Makefile.in Makefile.include.in contrib/Makefile.in contrib/emacs/t-mouse.el doc/Makefile.in doc/doc.gpm.in doc/manpager src/Makefile.in
Copyright: 1993       Andrew Haylett <ajh@gec-mrc.co.uk>
 1994-2000  Alessandro Rubini <rubini@linux.it>
 1998-1999  Ian Zimmerman <itz@rahul.net>
 2001-2012  Nico Schottelius <nico@schottelius.org>
License: GPL-2.0+
